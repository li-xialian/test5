package com.lxl.service;

import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.po.TbControl;
import com.lxl.model.po.TbDevice;
import com.lxl.model.po.TbWarning;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxl
 * @since 2023-07-25
 */
public interface ContentService {
    boolean publish(LocalDateTime now, LocalDateTime end);

    boolean pubTime(TbControl tb);

    PageResult<TbDevice> selectSearch(PageParams pageParams, String keyword);

    Boolean addIndex(TbDevice tbDevice);

    Boolean delIndex(String batteryid);

    Boolean updataIndex(TbDevice tbDevice);

    PageResult<TbWarning> getVStatus(PageParams pageParams,String start, String end);

//    /**
//     * 分页无条件查询
//     * @return 查询的储运厢表结果
//     */
//    List<TbStorage> getTbStorage();
//
//
//    List<TbContorl> getContorl();
//
//    /**
//     * 查询实时光照数据
//
//     * @param start
//     * @param end
//     * @return
//     */
////    CharDto getRealLight(String start, String end);
//    List<TbReall> getRealLight(String start, String end);
//
//    /**
//     * 查询实时温度数据
//
//     * @param start
//     * @param end
//     * @return
//     */
////    CharDto getRealTemperature(String start, String end);
//    List<TbRealt> getRealTemperature(String start, String end);
//
//    /**
//     * 查询设备状态
//
//     * @param start
//     * @param end
//     * @return
//     */
//    List<TbVstatus> getVStatus( String start, String end);

}
