package com.lxl.service;

import com.lxl.model.dto.Option;
import com.lxl.model.dto.QueryDto;
import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.po.TbCard;
import com.lxl.model.po.TbData;
import com.lxl.model.po.TbOther;

import java.util.List;

public interface QueryService {
    //历史查询
    public PageResult<TbCard> getData(PageParams pageParams, DateTimeDtos dateTimeDtos);

    //    查询设备
    PageResult<TbData> getReallyData(PageParams pageParams, DateTimeDtos dateTimeDtos);

    public PageResult<TbCard> getCard(PageParams pageParams, DateTimeDtos dateTimeDtos);
    public List<Option> getOption1() ;
    public List<Option> getOption2() ;

    List<Option> getOption11();
    List<Option> getOption22();

    TbCard geta(String id1,String id2,DateTimeDtos dateTimeDtos);

    PageResult<TbOther> getOther(PageParams pageParams, DateTimeDtos dateTimeDtos);

//    List<Option> getOptionAll();
}
