package com.lxl.service;


import com.lxl.model.po.*;

public interface TbSaveService {


    Boolean addIndex(TbDevice tbDevice);
    int saveData(TbData tbData);

    int saveOther(TbOther tbOther);

    int saveCard(TbCard tbCard);

    int saveWarning(TbWarning tbWarning);

    int saveControl(TbControl tb);

    Boolean updataDevice(TbDevice tbDevice);
    Boolean saveCid(TbCid tbCid);
}
