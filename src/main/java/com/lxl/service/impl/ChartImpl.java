package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.TbCardMapper;
import com.lxl.mapper.TbDataMapper;
import com.lxl.mapper.TbOtherMapper;
import com.lxl.model.dto.chart.CharDto;
import com.lxl.model.dto.chart.SeriesDto;
import com.lxl.model.po.TbCard;
import com.lxl.model.po.TbData;
import com.lxl.model.po.TbOther;
import com.lxl.service.ChartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ChartImpl implements ChartService {
    @Autowired
    TbDataMapper tbDataMapper;
    @Autowired
    TbCardMapper tbCardMapper;
    @Autowired
    TbOtherMapper tbOtherMapper;
    @Value("${lxl.colTime}")
    private String colTime;
    @Value("${lxl.colDeviceId}")
    private String colDeviceId;
    @Value("${lxl.colcardId}")
    private String colcardId;

    @Override
    public CharDto chat1(Integer size, Long id) {
        CharDto charDto = new CharDto();

        QueryWrapper<TbOther> wrapper1 = new QueryWrapper<TbOther>();
        wrapper1.orderByDesc(colTime)
                .eq(colDeviceId, id)
                .last("LIMIT " + size);
        List<TbOther> tb1 = tbOtherMapper.selectList(wrapper1);

        //赋值1
//        List<Float> c1 = tb1.stream().map(TbOther::getDevicetotal).collect(Collectors.toList());
//        List<Float> c2 = tb1.stream().map(TbOther::getT).collect(Collectors.toList());
        List<String> x = tb1.stream().map(TbOther::getX).collect(Collectors.toList());
        List<String> y = tb1.stream().map(TbOther::getY).collect(Collectors.toList());
        List<LocalDateTime> time1 = tb1.stream().map(TbOther::getTime).collect(Collectors.toList());

//        Collections.reverse(c1);
        Collections.reverse(x);
        Collections.reverse(y);
        Collections.reverse(time1);

        List<SeriesDto> series1 = new ArrayList<>();

        for (int j = 0; j < 2; j++) {
            SeriesDto<String> s1 = new SeriesDto<>();
            if (j == 0) {
                s1.setName("ts1");
                s1.setData(x);
            }
            if (j == 1) {
                s1.setName("ts2");
                s1.setData(y);
            }
            series1.add(s1);
        }

        charDto.setSeries(series1);
        charDto.setCategories(time1);

        System.out.println("表1aaaaa:" + charDto);
        return charDto;
    }

    @Override
    public CharDto chat2(Integer size, Long id) {
        CharDto charDto = new CharDto();

        QueryWrapper<TbCard> wrapper1 = new QueryWrapper<TbCard>();
        wrapper1.orderByDesc(colTime)
                .eq(colcardId, id)
                .last("LIMIT " + size);
        List<TbCard> tb1 = tbCardMapper.selectList(wrapper1);


        //赋值1
        List<Float> v1 = tb1.stream().map(TbCard::getV1).collect(Collectors.toList());
        List<Float> v2 = tb1.stream().map(TbCard::getV2).collect(Collectors.toList());
        List<Float> t1 = tb1.stream().map(TbCard::getT1).collect(Collectors.toList());
        List<Float> t2 = tb1.stream().map(TbCard::getT2).collect(Collectors.toList());
        List<Float> t = tb1.stream().map(TbCard::getT).collect(Collectors.toList());
        List<Float> i = tb1.stream().map(TbCard::getI).collect(Collectors.toList());
        List<String> vstatus = tb1.stream().map(TbCard::getVstatus).collect(Collectors.toList());
        List<Float> total = tb1.stream().map(TbCard::getCardtotal).collect(Collectors.toList());
        List<LocalDateTime> time1 = tb1.stream().map(TbCard::getTime).collect(Collectors.toList());

        Collections.reverse(total);
        Collections.reverse(time1);

        List<SeriesDto> series1 = new ArrayList<>();
        for (int j = 0; j < 1; j++) {
            SeriesDto<Float> s1 = new SeriesDto<Float>();
            if (j == 0) {
                s1.setName("饮水总量");
                s1.setData(total);
            }
            series1.add(s1);
        }

        charDto.setSeries(series1);
        charDto.setCategories(time1);

        System.out.println("表1aaaaa:" + charDto);
        return charDto;
    }


}
