package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxl.mapper.TbControlMapper;
import com.lxl.mapper.TbDeviceMapper;
import com.lxl.mapper.TbWarningMapper;
import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.po.TbControl;
import com.lxl.model.po.TbDevice;
import com.lxl.model.po.TbWarning;
import com.lxl.mqtt.MqttMessageService;
import com.lxl.service.ContentService;
import com.lxl.service.TbSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxl
 */
@Slf4j
@Service
public class ContentInterImpl implements ContentService {

    @Autowired
    TbDeviceMapper tbDeviceMapper;

    @Autowired
    TbWarningMapper tbWarningMapper;

    @Autowired
    MqttMessageService mqttMessageService;
    @Autowired
    TbControlMapper tbControlMapper;
    @Autowired
    TbSaveService tbSaveService;

    @Value("${lxl.colTime}")
    private String colTime;
    @Value("${lxl.colDeviceId}")
    private String colDeviceId;
    @Value("${lxl.colcardId}")
    private String colcardId;
    @Value("${topic.control}")
    private String topic;
    @Value("${lxl.colStatus}")
    private String colStatus;

    //发送
    @Override
    public boolean publish(LocalDateTime now, LocalDateTime end) {
        List<TbControl> durContorl = getDurContorl(now, end);
        System.out.println(durContorl);
        if (durContorl != null) {
            for (TbControl tb : durContorl) {
                mqttMessageService.publish(topic, tb);
            }
        }
        return true;
    }

    @Override
    public boolean pubTime(TbControl tb) {
        mqttMessageService.publish(topic, tb);
        return true;
    }

    /**
     * @param now
     * @param end
     * @return 一段时间的控制数据
     */
//    没有接口调用
    private List<TbControl> getDurContorl(LocalDateTime now, LocalDateTime end) {
        QueryWrapper<TbControl> wrapper = new QueryWrapper<>();
        wrapper.ge(colTime, now)
                .le(colTime, end);
        List<TbControl> tb = tbControlMapper.selectList(wrapper);
        return tb;
    }

    //    搜索，设备一览
    @Override
    public PageResult<TbDevice> selectSearch(PageParams pageParams, String id) {
        QueryWrapper<TbDevice> wrapper = new QueryWrapper<>();
        wrapper.like(null != id, colDeviceId, id)
                .or()
                .like(null != id,"massege",id);
//上面是根据或者massege模糊查询
        Page<TbDevice> page = new Page<>();
        if (null != pageParams.getPageCurrent()) {
            page = new Page<>(pageParams.getPageCurrent(), pageParams.getPageSize());
        }
        Page<TbDevice> tbDevicePage = tbDeviceMapper.selectPage(page, wrapper);
        List<TbDevice> records = tbDevicePage.getRecords();
        long total = tbDevicePage.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        PageResult<TbDevice> t;
        if (null != pageParams.getPageCurrent()) {
            t = new PageResult<>(records, total, pageParams.getPageCurrent(), pageParams.getPageSize());
        } else {
            t = new PageResult<>(records);
        }

        return t;
    }

    /**
     * 添加操作加判断
     *
     * @param
     * @return 不存在就插入，插入成功返回true，插入失败或者存在返回false
     */
    @Override
    public Boolean addIndex(TbDevice tbDevice) {
        return tbSaveService.addIndex(tbDevice);
    }


    @Override
    public Boolean delIndex(String id) {
        QueryWrapper<TbDevice> wrapper = new QueryWrapper<>();
        wrapper.eq(colDeviceId, id);
        int i = tbDeviceMapper.delete(wrapper);
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean updataIndex(TbDevice tbDevice) {
        Boolean aBoolean = tbSaveService.updataDevice(tbDevice);
        return aBoolean;
    }

    //    得到状态信息,运行状态栏
    @Override
    public PageResult<TbWarning> getVStatus(PageParams pageParams, String start, String end) {
        QueryWrapper<TbWarning> wrapper = new QueryWrapper<>();
        if (start != null && end != null) {
            wrapper.ge(colTime, start)
                    .le(colTime, end)
                    .orderByDesc(colTime);
        }
        Page<TbWarning> page = new Page<>();
        if (null != pageParams.getPageCurrent()) {
            page = new Page<>(pageParams.getPageCurrent(), pageParams.getPageSize());
        }
        Page<TbWarning> tbReallPage = tbWarningMapper.selectPage(page, wrapper);
        List<TbWarning> items = tbReallPage.getRecords();
        long total = tbReallPage.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        PageResult<TbWarning> tbReallPageResult;
        if (null != pageParams.getPageCurrent()) {
            tbReallPageResult = new PageResult<>(items, total, pageParams.getPageCurrent(), pageParams.getPageSize());
        } else {
            tbReallPageResult = new PageResult<>(items);
        }

        return tbReallPageResult;

    }

}
