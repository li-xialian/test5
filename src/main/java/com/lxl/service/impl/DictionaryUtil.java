package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.DictionaryMapper;
import com.lxl.model.po.Dictionary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DictionaryUtil {
    @Autowired
    DictionaryMapper dictionaryMapper;

    public String getDictionnary(String id){
        QueryWrapper<Dictionary> wrapper=new QueryWrapper<>();
        wrapper.eq("id",id);
        Integer integer = dictionaryMapper.selectCount(wrapper);
        String flag="kong";
        if(0!=integer){
            Dictionary dictionary = dictionaryMapper.selectOne(wrapper);
            flag = dictionary.getFlag();
        }
        return flag;
    }
}
