package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.TbUserMapper;
import com.lxl.model.po.TbUser;
import com.lxl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;


@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    TbUserMapper tbUserMapper;

    @Override
    public String selectUser(TbUser tbUser) {
        if(null!=tbUser.getPassword()) {

            String password = tbUser.getPassword();
            byte[] bytes = password.getBytes();
            String md5Hex = DigestUtils.md5DigestAsHex(bytes);

            QueryWrapper<TbUser> wrapper = new QueryWrapper<>();
            wrapper.eq("account", tbUser.getAccount())
                    .eq("password", md5Hex);

            TbUser u = tbUserMapper.selectOne(wrapper);
            if (null != u) {

                return "success";
            }
        }
        return "false";
    }

    @Override
    public String addUesr(TbUser tbUser) {

        if(""!=tbUser.getAccount()&&""!=tbUser.getPassword()) {

            String password= tbUser.getPassword();
            byte[] bytes = password.getBytes();
            String md5Hex = DigestUtils.md5DigestAsHex(bytes);

            QueryWrapper<TbUser> wrapper = new QueryWrapper<>();
            wrapper.eq("account", tbUser.getAccount())
                    .eq("password", md5Hex);
            TbUser u = tbUserMapper.selectOne(wrapper);
            int i = 0;
            if (null == u) {
                tbUser.setPassword(md5Hex);
                i = tbUserMapper.insert(tbUser);
            }
            if (i > 0) {
                return "success";
            }

        }
        return "false";
    }
}
