package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.*;
import com.lxl.model.po.*;
import com.lxl.service.TbSaveService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class TbSaveImpl implements TbSaveService {

    @Autowired
    TbDeviceMapper tbDeviceMapper;
    @Autowired
    TbCardMapper tbCardMapper;
    @Autowired
    TbControlMapper tbControlMapper;
    @Autowired
    TbDataMapper tbDataMapper;
    @Autowired
    TbWarningMapper tbWarningMapper;
    @Autowired
    TbCidMapper tbCidMapper;
    @Autowired
    DictionaryUtil dictionaryUtil;
    @Autowired
    TbOtherMapper tbOtherMapper;

    @Value("${lxl.colTime}")
    private String colTime;
    @Value("${lxl.colDeviceId}")
    private String colDeviceId;
    @Value("${lxl.colcardId}")
    private String colcardId;
    @Value("${lxl.id}")
    private String colId;


    @Override
    public int saveData(TbData tbData) {
        int insert = 2;
//        if(tbData.getDevicetotal()!=0) {
//            Long deviceid = tbData.getDeviceid();
//            Float totalnew = tbData.getDevicetotal();
//            if (null != totalnew) {
//                QueryWrapper<TbData> wrapper = new QueryWrapper<>();
////        最新的一条加上值
//                wrapper.eq(colDeviceId, deviceid)
//                        .orderByDesc(colId)
//                        .last("LIMIT " + 1);
//                Integer integer = tbDataMapper.selectCount(wrapper);
//                if (integer > 0) {
//                    TbData tbExist = tbDataMapper.selectOne(wrapper);
//                    Float total = tbExist.getDevicetotal();
//                    tbData.setDevicetotal(total + totalnew);
//                }
//            }
//             insert = tbDataMapper.insert(tbData);
//        }
        return insert;
    }

    @Override
    public int saveOther(TbOther tbOther) {
        int insert = tbOtherMapper.insert(tbOther);
        return insert;
    }

    @Override
    public int saveCard(TbCard tbCard) {
        int insert = 2;
        if (tbCard.getCardtotal() != 0) {
            Long cardid = tbCard.getCardid();
            Long deviceid = tbCard.getDeviceid();
            Float cardTotalnew = tbCard.getCardtotal();
            Float devicetotal =cardTotalnew;
            Float cardtotal =cardTotalnew;
            QueryWrapper<TbCard> wrapper = new QueryWrapper<>();
//        最新的一条加上值
            wrapper.eq(colcardId, cardid)
                    .orderByDesc(colId)
                    .last("LIMIT " + 1);

            QueryWrapper<TbCard> wrapper2 = new QueryWrapper<>();
//        最新的一条加上值
            wrapper2.eq(colDeviceId, deviceid)
                    .orderByDesc(colId)
                    .last("LIMIT " + 1);

            Integer integer = tbCardMapper.selectCount(wrapper);
            Integer integer2 = tbCardMapper.selectCount(wrapper2);

            if(integer2>0) {
                TbCard tbCard1 = tbCardMapper.selectOne(wrapper2);
                Float devicetotalT = tbCard1.getDevicetotal();
                if (null != devicetotalT) {
                    devicetotal = cardTotalnew + devicetotalT;
                }
            }
            tbCard.setDevicetotal(devicetotal);
            tbCard.setT(cardTotalnew);
            if (integer > 0) {
                TbCard tbExist = tbCardMapper.selectOne(wrapper);
                Float cardtotalT = tbExist.getCardtotal();
//                Float devicetotal = tbExist.getDevicetotal();
                if (null != cardtotal) {
                   cardtotal=cardTotalnew+cardtotalT;
                }
//                else{
//                    tbCard.setCardtotal(cardTotalnew);
//                }
//                if (null != devicetotal) {
//                    tbCard.setDevicetotal(devicetotal + cardTotalnew);
//                } else {
//                    tbCard.setDevicetotal(cardTotalnew);
//                }

//            } else {
//                tbCard.setCardtotal(cardTotalnew);
////                tbCard.setDevicetotal(cardTotalnew);
//
            }
            tbCard.setCardtotal(cardtotal);
            insert = tbCardMapper.insert(tbCard);
        }
        return insert;
    }

    @Override
    public int saveWarning(TbWarning tb) {
//        String dictionnary = dictionaryUtil.getDictionnary(tb.getStatus2());
//        if(!"kong".equals(dictionnary)){
//            tb.setStatus2(dictionnary);
//        }
//        String dictionnary2 = dictionaryUtil.getDictionnary(tb.getMassege());
//        if(!"kong".equals(dictionnary2)){
//            tb.setMassege(dictionnary2);
//        }
        LocalDateTime time2 = tb.getTime();
        String t2= String.valueOf(time2);
        int i = t2.indexOf(".");
        String t3 = t2.substring(0, i);
        int insert =0;
        QueryWrapper<TbWarning> wrapper = new QueryWrapper<>();
//        最新的一条加上值
        wrapper.orderByDesc(colTime)
                .eq(colTime,t3)
                .last("LIMIT " + 1);
        try {
            TbWarning tbWarning1 = tbWarningMapper.selectOne(wrapper);
            LocalDateTime time1 = tbWarning1.getTime();
            String t1= String.valueOf(time1);

            if(t3.equals(t1)){
                return 2;
            }
            insert=tbWarningMapper.insert(tb);
        }catch (Exception e){
           insert=tbWarningMapper.insert(tb);
        }
        return insert;
    }

    @Override
    public int saveControl(TbControl tb) {
        String dictionnary = dictionaryUtil.getDictionnary(tb.getStatus());
        if (!"kong".equals(dictionnary)) {
            tb.setStatus(dictionnary);
        }
        int insert = tbControlMapper.insert(tb);
        return insert;
    }

    @Override
    public Boolean updataDevice(TbDevice tbDevice) {
        Long deviceid = tbDevice.getDeviceid();

//        不存在就插入，插入成功返回true，插入失败或者存在返回false
        Boolean existBool = addIndex(tbDevice);
//        如果存在下面返回false，进行更新操作
        if (!existBool) {
            String a2 = tbDevice.getLocation();
            String a3 = tbDevice.getStatus();
            String a4 = tbDevice.getMassege();
            String x = tbDevice.getX();
            String y = tbDevice.getY();

//        更新规则
            QueryWrapper<TbDevice> wrapper = new QueryWrapper<>();
            wrapper.eq(colDeviceId, deviceid);
            TbDevice tb = tbDeviceMapper.selectOne(wrapper);
//        更新要注意有些更新的属性未空的则不更新
            if (!StringUtils.isBlank(a2) && !"".equals(a2)) {
                String dictionnary = dictionaryUtil.getDictionnary(tbDevice.getLocation());
                if (!"kong".equals(dictionnary)) {
                    tb.setLocation(dictionnary);
                } else {
                    tb.setLocation(a2);
                }
            }
            if (!StringUtils.isBlank(a3) && !"".equals(a3)) {
                String dictionnary = dictionaryUtil.getDictionnary(tbDevice.getStatus());
                if (!"kong".equals(dictionnary)) {
                    tb.setStatus(dictionnary);
                } else {
                    tb.setStatus(a2);
                }
            }
            if (!StringUtils.isBlank(a4)&&!"".equals(a4)) {
//                String dictionnary = dictionaryUtil.getDictionnary(tbDevice.getMassege());
//                if(!"kong".equals(dictionnary)){
//                    tb.setMassege(dictionnary);
//                }else {
//                    tb.setMassege(a4);
//                }
                tb.setMassege(a4);
            }
            if (!StringUtils.isBlank(x)&&!"".equals(x)) {
                tb.setX(x);
            }
            if (!StringUtils.isBlank(y)&&!"".equals(y)) {
                tb.setY(y);
            }
            int i = tbDeviceMapper.update(tb, wrapper);
            if (i > 0) {
                existBool = true;
            } else {
                existBool = false;
            }
        }
        return existBool;
    }


    @Override
    public Boolean saveCid(TbCid tbCid) {
        Long cardid = tbCid.getCardid();

        QueryWrapper<TbCid> wrapper = new QueryWrapper<>();
        wrapper.eq(colcardId, cardid);
        int exist = tbCidMapper.selectCount(wrapper);

        int i = 0;
        //不存在则添加
        if (0 == exist && cardid != 0) {
            i = tbCidMapper.insert(tbCid);
        }
        if (i > 0) {
            return true;
        } else {
            return false;
        }

    }

    public Boolean addIndex(TbDevice tbDevice) {
        Long deviceid = tbDevice.getDeviceid();

        QueryWrapper<TbDevice> wrapper = new QueryWrapper<>();
        wrapper.eq(colDeviceId, deviceid);
        int exist = tbDeviceMapper.selectCount(wrapper);

        int i = 0;
        //不存在则添加
        if (0 == exist) {
            String dictionnary1 = dictionaryUtil.getDictionnary(tbDevice.getStatus());
            if (!"kong".equals(dictionnary1)) {
                tbDevice.setStatus(dictionnary1);
            }
            String dictionnary2 = dictionaryUtil.getDictionnary(tbDevice.getLocation());
            if (!"kong".equals(dictionnary2)) {
                tbDevice.setLocation(dictionnary2);
            }
//            String dictionnary3 = dictionaryUtil.getDictionnary(tbDevice.getMassege());
//            if(!"kong".equals(dictionnary3)){
//                tbDevice.setMassege(dictionnary3);
//            }
            tbDevice.setX(tbDevice.getX());
            tbDevice.setY(tbDevice.getY());
            i = tbDeviceMapper.insert(tbDevice);
        }
        if (i > 0) {
            return true;
        } else {
            return false;
        }

    }

    //    @Override
//    public Boolean updataCID(TbCid tbCid) {
//        Long cardid = tbCid.getCardid();
//
////        不存在就插入，插入成功返回true，插入失败或者存在返回false
//        Boolean existBool = addCard(tbCid);
////        如果存在下面返回false，进行更新操作
//        if (!existBool) {
//            Long a2 = tbCid.getCardid();
//            Long a3 = tbCid.getDeviceid();
//            String a4 = tbCid.getMassege();
////        更新规则
//            QueryWrapper<TbCid> wrapper = new QueryWrapper<>();
//            wrapper.eq(colDeviceId, cardid);
////        更新要注意有些更新的属性未空的则不更新
//            if (null != a2) {
//                tbCid.setCardid(a2);
//            }
//            if (null != a3) {
//                tbCid.setDeviceid(a3);
//            }
//            if (!StringUtils.isBlank(a4)) {
//                tbCid.setMassege(a4);
//            }
//            int i = tbCidMapper.update(tbCid, wrapper);
//            if (i > 0) {
//                existBool = true;
//            } else {
//                existBool = false;
//            }
//        }
//        return existBool;
//    }
}
