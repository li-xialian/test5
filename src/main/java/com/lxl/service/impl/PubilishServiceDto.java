package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.TbControlMapper;
import com.lxl.model.po.TbControl;
import com.lxl.mqtt.MqttMessageService;
import com.lxl.service.PubilishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class PubilishServiceDto implements PubilishService {

    @Autowired
    MqttMessageService mqttMessageService;
    @Autowired
    TbControlMapper tbControlMapper;

    @Override
    public boolean publish(LocalDateTime now, LocalDateTime end) {
        List<TbControl> durContorl = getDurContorl(now, end);
        System.out.println(durContorl);
        if (durContorl != null) {
            for (TbControl tb : durContorl) {
                mqttMessageService.publish("mqtt/Control", tb);
            }
        }
        return true;
    }

    private List<TbControl> getDurContorl(LocalDateTime now, LocalDateTime end) {
        QueryWrapper<TbControl> wrapper = new QueryWrapper<>();
        wrapper.ge("time", now)
                .le("time", end)
                .orderByDesc("time");
        List<TbControl> tb = tbControlMapper.selectList(wrapper);
        return tb;
    }
}
