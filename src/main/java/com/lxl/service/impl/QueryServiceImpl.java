package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxl.mapper.*;
import com.lxl.model.dto.Option;
import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.po.*;
import com.lxl.service.QueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class QueryServiceImpl implements QueryService {

    @Autowired
    TbCidMapper tbCidMapper;
    @Autowired
    TbCardMapper tbCardMapper;

    @Autowired
    TbDeviceMapper tbDeviceMapper;
    @Autowired
    TbDataMapper tbDataMapper;
    @Autowired
    TbOtherMapper tbOtherMapper;

    @Value("${lxl.colTime}")
    private String colTime;
    @Value("${lxl.colDeviceId}")
    private String colDeviceId;
    @Value("${lxl.colcardId}")
    private String colcardId;

    @Override
    public PageResult<TbCard> getData(PageParams pageParams, DateTimeDtos dateTimeDtos) {
        String id1 = dateTimeDtos.getId1();
        QueryWrapper<TbCard> wrapper = new QueryWrapper<>();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        Long pageCurrent = pageParams.getPageCurrent();
        Long pageSize = pageParams.getPageSize();
//        all则查询全部
        if (!"查询所有".equals(id1)) {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .eq(colDeviceId, id1)
                        .orderByDesc(colTime);
            }
        } else {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .orderByDesc(colTime);
            }
        }
//        分页处理
        PageResult<TbCard> tp;
        Page<TbCard> page = new Page<>();
        if (null != pageCurrent) {
            page = new Page<>(pageCurrent, pageSize);
        }
        Page<TbCard> t = tbCardMapper.selectPage(page, wrapper);
        List<TbCard> records = t.getRecords();
        long total = t.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        if (null != pageCurrent) {
            tp = new PageResult<>(records, total, pageCurrent, pageSize);
        } else {
            tp = new PageResult<>(records);
        }

        return tp;
    }
//    查询设备
    @Override
    public PageResult<TbData> getReallyData(PageParams pageParams, DateTimeDtos dateTimeDtos) {
        String id1 = dateTimeDtos.getId1();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        Long pageCurrent = pageParams.getPageCurrent();
        Long pageSize = pageParams.getPageSize();
        QueryWrapper<TbData> wrapper = new QueryWrapper<>();
        if (!"查询所有".equals(id1)) {
            //        查询指定设备
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .eq(colDeviceId, id1)
                        .orderByDesc(colTime);
            }
        } else {
//            查询全部
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .orderByDesc(colTime);
            }
        }
//        分页处理
        PageResult<TbData> tp;
        Page<TbData> page = new Page<>();
        if (null != pageCurrent) {
            page = new Page<>(pageCurrent, pageSize);
        }
        Page<TbData> t = tbDataMapper.selectPage(page, wrapper);
        List<TbData> records = t.getRecords();
        long total = t.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        if (null != pageCurrent) {
            tp = new PageResult<>(records, total, pageCurrent, pageSize);
        } else {
            tp = new PageResult<>(records);
        }

        return tp;
    }


    @Override
    public PageResult<TbCard> getCard(PageParams pageParams, DateTimeDtos dateTimeDtos) {
        QueryWrapper<TbCard> wrapper = new QueryWrapper<>();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        Long pageCurrent = pageParams.getPageCurrent();
        Long pageSize = pageParams.getPageSize();
        String id2 = dateTimeDtos.getId2();


        if (!"查询所有".equals(id2)) {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .eq(colcardId, id2)
                        .orderByDesc(colTime);
            }
        } else {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .orderByDesc(colTime);
            }
        }
        Page<TbCard> page = new Page<>();
        if (null != pageCurrent) {
            page = new Page<>(pageCurrent, pageSize);
        }
        Page<TbCard> t = tbCardMapper.selectPage(page, wrapper);
        List<TbCard> records = t.getRecords();
        long total = t.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        PageResult<TbCard> tp;
        if (null != pageCurrent) {
            tp = new PageResult<>(records, total, pageCurrent, pageSize);
        } else {
            tp = new PageResult<>(records);
        }

        return tp;
    }

    @Override
    public List<Option> getOption1() {
        LambdaQueryWrapper<TbDevice> wrapper = new LambdaQueryWrapper<>();
        List<TbDevice> tbBikes = tbDeviceMapper.selectList(wrapper);
        List<Option> pList = new ArrayList<>();
        List<Long> collect = tbBikes.stream().map(TbDevice::getDeviceid).collect(Collectors.toList());
        int i = 1;
        for (Long c : collect) {
            Option p = new Option();
            p.setLabel(String.valueOf(c));
            p.setValue(i);
            pList.add(p);
            i++;
        }
        Option p = new Option();
        p.setLabel("查询所有");
        p.setValue(i);
        pList.add(p);
        return pList;
    }

    @Override
    public List<Option> getOption2() {
        LambdaQueryWrapper<TbCid> wrapper = new LambdaQueryWrapper<>();
        List<TbCid> t = tbCidMapper.selectList(wrapper);
        List<Option> pList = new ArrayList<>();
        List<Long> collect = t.stream().map(TbCid::getCardid).collect(Collectors.toList());
        int i = 1;
        for (Long c : collect) {
            Option p = new Option();
            p.setLabel(String.valueOf(c));
            p.setValue(i);
            pList.add(p);
            i++;
        }
        Option p = new Option();
        p.setLabel("查询所有");
        p.setValue(i);
        pList.add(p);
        return pList;
    }

    @Override
    public List<Option> getOption11() {
        LambdaQueryWrapper<TbDevice> wrapper = new LambdaQueryWrapper<>();
        List<TbDevice> tbBikes = tbDeviceMapper.selectList(wrapper);
        List<Option> pList = new ArrayList<>();
        List<Long> collect = tbBikes.stream().map(TbDevice::getDeviceid).collect(Collectors.toList());
        int i = 1;
        for (Long c : collect) {
            Option p = new Option();
            p.setLabel(String.valueOf(c));
            p.setValue(i);
            pList.add(p);
            i++;
        }
        return pList;
    }

    @Override
    public List<Option> getOption22() {
        LambdaQueryWrapper<TbCid> wrapper = new LambdaQueryWrapper<>();
        List<TbCid> t = tbCidMapper.selectList(wrapper);
        List<Option> pList = new ArrayList<>();
        List<Long> collect = t.stream().map(TbCid::getCardid).collect(Collectors.toList());
        int i = 1;
        for (Long c : collect) {
            Option p = new Option();
            p.setLabel(String.valueOf(c));
            p.setValue(i);
            pList.add(p);
            i++;
        }
        return pList;
    }

    @Override
    public TbCard geta(String id1, String id2,DateTimeDtos dateTimeDtos) {
        TbCard tbCard = new TbCard();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        tbCard.setDeviceid(Long.valueOf(id1));
        tbCard.setCardid(Long.valueOf(id2));
        QueryWrapper<TbCard> wrapper = new QueryWrapper<>();
        wrapper.eq(colcardId, id2)
                .ge(colTime, start)
                .le(colTime, end)
                .orderByDesc("id")
                .last("LIMIT " + 1);
        Integer integer = tbCardMapper.selectCount(wrapper);

        QueryWrapper<TbCard> wrapper2 = new QueryWrapper<>();
        wrapper2.eq(colDeviceId, id1)
                .orderByDesc("id")
                .last("LIMIT " + 1);
        Integer integer2 = tbCardMapper.selectCount(wrapper2);
        Float devicetotal = 0F;
        if(integer2>0){
            TbCard tbCard1 = tbCardMapper.selectOne(wrapper2);
             devicetotal = tbCard1.getDevicetotal();
        }
        if (integer > 0) {
            TbCard tbCard2 = tbCardMapper.selectOne(wrapper);
            Float cardtotal = tbCard2.getCardtotal();
            tbCard.setDevicetotal(devicetotal);
            tbCard.setCardtotal(cardtotal);
        } else {
            tbCard.setDevicetotal((float) 0);
            tbCard.setCardtotal((float) 0);

        }
        return tbCard;
    }

//    查询第三个表
    @Override
    public PageResult<TbOther> getOther(PageParams pageParams, DateTimeDtos dateTimeDtos) {
        String id1 = dateTimeDtos.getId1();
        QueryWrapper<TbOther> wrapper = new QueryWrapper<>();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        Long pageCurrent = pageParams.getPageCurrent();
        Long pageSize = pageParams.getPageSize();
//        all则查询全部
        if (!"查询所有".equals(id1)) {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .eq(colDeviceId, id1)
                        .orderByDesc(colTime);
            }
        } else {
            if (start != null && end != null) {
                wrapper.ge(colTime, start)
                        .le(colTime, end)
                        .orderByDesc(colTime);
            }
        }
//        分页处理
        PageResult<TbOther> tp;
        Page<TbOther> page = new Page<>();
        if (null != pageCurrent) {
            page = new Page<>(pageCurrent, pageSize);
        }
        Page<TbOther> t = tbOtherMapper.selectPage(page, wrapper);
        List<TbOther> records = t.getRecords();
        long total = t.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        if (null != pageCurrent) {
            tp = new PageResult<>(records, total, pageCurrent, pageSize);
        } else {
            tp = new PageResult<>(records);
        }

        return tp;
    }

//    @Override
//    public List<Option> getOptionAll() {
//        LambdaQueryWrapper<TbBike> wrapper = new LambdaQueryWrapper<>();
//        List<TbBike> tbBikes = tbBikeMapper.selectList(wrapper);
//        List<Option> pList = new ArrayList<>();
//        List<Long> collect = tbBikes.stream().map(TbBike::getCycleid).collect(Collectors.toList());
//        int i = 1;
//        for (Long c : collect) {
//            Option p = new Option();
//            p.setLabel(String.valueOf(c));
//            p.setValue(i);
//            pList.add(p);
//            i++;
//        }
//        Option p = new Option();
//        p.setLabel("all");
//        p.setValue(i);
//        pList.add(p);
//        return pList;
//    }
}
