package com.lxl.service;

import java.time.LocalDateTime;

public interface PubilishService {

    boolean publish(LocalDateTime now, LocalDateTime end);
}
