package com.lxl.mapper;

import com.lxl.model.po.TbOther;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 */
public interface TbOtherMapper extends BaseMapper<TbOther> {

}
