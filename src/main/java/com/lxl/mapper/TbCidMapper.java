package com.lxl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxl.model.po.TbCid;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2023-08-24
 */
public interface TbCidMapper extends BaseMapper<TbCid> {

}
