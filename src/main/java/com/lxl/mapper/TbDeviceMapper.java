package com.lxl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxl.model.po.TbDevice;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
public interface TbDeviceMapper extends BaseMapper<TbDevice> {

}
