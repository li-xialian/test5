package com.lxl.mqtt;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxl.mapper.TbControlMapper;
import com.lxl.mapper.TbDeviceMapper;
import com.lxl.model.dto.Option;
import com.lxl.model.dto.mqttMas.TbControlDto;
import com.lxl.model.po.TbControl;
import com.lxl.model.po.TbData;
import com.lxl.model.po.TbDevice;
import com.lxl.service.QueryService;
import com.lxl.utils.GetDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * MQTT工具类操作
 *
 * @author Mr.Qu
 * @since 2020/11/18
 */
@Slf4j
@Component
public class MQTTConnect {

    @Value("${mqtt.broker}")
    private String HOST;
    private final String clientId = "Client" + (int) (Math.random() * 100000000);
    private MqttClient mqttClient;
    private GetDataUtil DataUtil = new GetDataUtil();
    @Autowired
    QueryService queryService;

    @Autowired
    TbControlMapper tbControlMapper;

    @Value("${lxl.colTime}")
    private String colTime;
    @Value("${lxl.colDeviceId}")
    private String colDeviceId;
    @Value("${lxl.colcardId}")
    private String colcardId;

    /**
     * 客户端connect连接mqtt服务器
     *
     * @param username     用户名
     * @param password     密码
     * @param mqttCallback 回调函数
     **/
    public void setMqttClient(String username, String password, MqttCallback mqttCallback)
            throws MqttException {
        MqttConnectOptions options = mqttConnectOptions(username, password);
        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.setCallback(mqttCallback);
        mqttClient.connect(options);
    }

    /**
     * 客户端connect连接mqtt服务器
     *
     * @param username 用户名
     * @param password 密码
     **/
    public void setMqttClient(String username, String password)
            throws MqttException {
        MqttConnectOptions options = mqttConnectOptions(username, password);
        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.connect(options);
    }

    /**
     * 客户端connect设置回调函数
     *
     * @param mqttCallback 回调函数
     **/
    public void setMqttClient(MqttCallback mqttCallback)
            throws MqttException {

        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.setCallback(mqttCallback);
    }


    /**
     * MQTT连接参数设置
     */
    private MqttConnectOptions mqttConnectOptions(String userName, String passWord)
            throws MqttException {
        mqttClient = new MqttClient(HOST, clientId, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(userName);
        options.setPassword(passWord.toCharArray());
        options.setConnectionTimeout(10);///默认：30
        options.setAutomaticReconnect(true);//默认：false
        options.setCleanSession(false);//默认：true
        //options.setKeepAliveInterval(20);//默认：60
        return options;
    }

    /**
     * 关闭MQTT连接
     */
    public void close() throws MqttException {
        mqttClient.close();
        mqttClient.disconnect();
    }

    /**
     * 向某个主题发布消息 默认qos：1
     */
    public void pub(String topic, String msg) throws MqttException {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(1);
        mqttMessage.setPayload(msg.getBytes());
        MqttTopic mqttTopic = mqttClient.getTopic(topic);
        MqttDeliveryToken token = mqttTopic.publish(mqttMessage);
        token.waitForCompletion();
    }

    /**
     * 向某个主题发布消息
     *
     * @param topic: 发布的主题
     * @param msg:   发布的消息
     * @param qos:   消息质量    Qos：0、1、2
     */
    public void pub(String topic, String msg, int qos) throws MqttException, InterruptedException {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(qos);
        MqttTopic mqttTopic = mqttClient.getTopic(topic);

        //一条一条发
        TbControl tb = JSON.parseObject(msg, TbControl.class);
        Long deviceid = tb.getDeviceid();
        String swit = tb.getSwit();
        Float light = tb.getLight();
        Float speed = tb.getSpeed();
        Integer flag = tb.getFlag();
        String fl = tb.getFl();
        String fs = tb.getFs();
        LocalDateTime time = tb.getTime();
        String status = tb.getStatus();
        Integer th = tb.getTh();

//        9999控制全部
        if (deviceid == 99999) {
            List<Option> option1 = queryService.getOption1();
            for (Option o : option1) {
                String label = o.getLabel();
                if (!"查询所有".equals(label)) {
                    if (null != status) {
                        if ("正常".equals(status)) {
                            mqttMessage.setPayload(("id:" + label + ",Normal").getBytes());
                            mqttTopic.publish(mqttMessage);
                            Thread.sleep(1000);
                        }
                        if ("复位".equals(status)) {
                            mqttMessage.setPayload(("id:" + label + ",time:" + DataUtil.getMillisData()).getBytes());
                            mqttTopic.publish(mqttMessage);
                            Thread.sleep(1000);
                            TbControl tbDevice = getControlLast();
                            if(null!=tbDevice) {
                                String status1 = tbDevice.getStatus();
                                if ("正常".equals(status1)) {
                                    mqttMessage.setPayload(("id:" + deviceid + ",Normal").getBytes());
                                }
                                if ("暂停".equals(status1)) {
                                    mqttMessage.setPayload(("id:" + deviceid + ",Pause").getBytes());
                                }

                                Thread.sleep(1000);
                            }
                        }
                        if ("暂停".equals(status)) {
                            mqttMessage.setPayload(("id:" + label + ",Pause").getBytes());
                            mqttTopic.publish(mqttMessage);
                            Thread.sleep(1000);
                        }
                    }
                    if (null != th && "true".equals(fs)) {
                        mqttMessage.setPayload(("id:" + label + ",TH:" + th).getBytes());
                        mqttTopic.publish(mqttMessage);
                        Thread.sleep(1000);
                    }
                }
            }
        } else {
            if (null != status) {
                if ("正常".equals(status)) {
                    mqttMessage.setPayload(("id:" + deviceid + ",Normal").getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                }
                if ("复位".equals(status)) {
                    mqttMessage.setPayload(("id:" + deviceid + ",time:" + DataUtil.getMillisData()).getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                    TbControl tbDevice = getControlLast();
                    if (null != tbDevice) {
                        String status1 = tbDevice.getStatus();
                        if ("正常".equals(status1)) {
                            mqttMessage.setPayload(("id:" + deviceid + ",Normal").getBytes());
                            mqttTopic.publish(mqttMessage);
                        }
                        if ("暂停".equals(status1)) {
                            mqttMessage.setPayload(("id:" + deviceid + ",Pause").getBytes());
                            mqttTopic.publish(mqttMessage);
                        }
                    }
                }
                if ("暂停".equals(status)) {
                    mqttMessage.setPayload(("id:" + deviceid + ",Pause").getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                }
            }
            if (null != th && "true".equals(fs)) {
                mqttMessage.setPayload(("id:" + deviceid + ",TH:" + th).getBytes());
                mqttTopic.publish(mqttMessage);
                Thread.sleep(1000);
            }
        }
    }

    public TbControl getControlLast() {
        QueryWrapper<TbControl> wrapper = new QueryWrapper<>();
        TbControl tbDevice = null;
        wrapper.ne("status", "复位")
                .orderByDesc(colTime)
                .last("LIMIT " + 1);
        try {
            tbDevice = tbControlMapper.selectOne(wrapper);
        } catch (Exception e) {
            return null;
        }
        return tbDevice;
    }


    /**
     * 订阅某一个主题 ，此方法默认的的Qos等级为：1
     *
     * @param topic 主题
     */
    public void sub(String topic) throws MqttException {
        mqttClient.subscribe(topic);
    }

    /**
     * 订阅某一个主题，可携带Qos
     *
     * @param topic 所要订阅的主题
     * @param qos   消息质量：0、1、2
     */
    public void sub(String topic, int qos) throws MqttException {
        mqttClient.subscribe(topic, qos);
    }

    public static void main(String[] args) throws MqttException {
//    MQTTConnect mqttConnect = new MQTTConnect();
//    String msg = "Mr.Qu" + (int) (Math.random() * 100000000);
//
//    mqttConnect.sub("com/iot/init");
//    mqttConnect.pub("com/iot/init", msg);


    }
}
