package com.lxl.mqtt;


import com.alibaba.fastjson.JSON;
import com.lxl.model.dto.Option;
import com.lxl.model.dto.mqttMas.TbCardDto;
import com.lxl.model.dto.mqttMas.TbDataDto;
import com.lxl.model.dto.mqttMas.TbOtherDto;
import com.lxl.model.dto.mqttMas.TbWarningDto;
import com.lxl.model.po.*;
import com.lxl.service.ContentService;
import com.lxl.service.QueryService;
import com.lxl.service.TbSaveService;
import com.lxl.utils.GetDataUtil;
import com.lxl.utils.ToJsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

/**
 * 回调函数
 */
@Slf4j
@Component
public class InitCallback implements MqttCallback {

    @Autowired
    TbSaveService tbSave;

    private String messageContent;
    @Autowired
    ContentService pubilishService;

    private GetDataUtil dateUtil = new GetDataUtil();

    @Value("${topic.device}")
    private String device;
    @Value("${topic.card}")
    private String card;
    @Value("${topic.warn}")
    private String warn;
    @Value("${topic.resume1}")
    private String resume1;
    @Value("${topic.resume2}")
    private String resume2;
    @Value("${topic.Redata}")
    private String Redata;

    @Autowired
    QueryService queryService;

    @Autowired
    TbSaveService tbSaveService;

    private static Long time1;
    private static Long time2;
    private static Long time3;

    private static LocalDateTime end = LocalDateTime.now();
    private static LocalDateTime now = LocalDateTime.now();

    private static LocalDateTime end2 = LocalDateTime.now();
    private static LocalDateTime now2 = LocalDateTime.now();


    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("connectionLost: " + cause.getMessage());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws InterruptedException {
        messageContent = new String(message.getPayload());
        //获取本地时间
        LocalDateTime localTime = LocalDateTime.now();
        //断网处理
        if (topic.equals(resume1)) {
            Boolean b = NetConnection(topic);
        }
        if (topic.equals(resume2)) {
            Boolean b = NetConnection2(topic);
        }
        //在这调用Service方法
        //判断Topic进行
        switch (topic) {
            case "mqtt/device":
                savedevice(messageContent);
                break;
            case "mqtt/card":
               savecard(messageContent);
                break;
            case "mqtt/warn":
               savewarn(messageContent);
                break;
            case "mqtt/Redata":
                savedevice(messageContent);
                savecard(messageContent);
                savewarn(messageContent);
                break;
            default:
                System.out.println("unknown topic");
                break;
        }

    }
    public void savedevice(String messageContent){
        String json = null;
        ToJsonUtils toJsonUtils = new ToJsonUtils();
        json = toJsonUtils.tojson(messageContent);
//                插入或更新设备
        TbDevice tbDevice = JSON.parseObject(json, TbDevice.class);
        Boolean aBoolean = tbSave.updataDevice(tbDevice);
//                插入设备数据
        TbDataDto tbDataDto = JSON.parseObject(json, TbDataDto.class);
        TbData tbDatanew = new TbData();
        BeanUtils.copyProperties(tbDataDto, tbDatanew);
        tbDatanew.setTime(dateUtil.getLocalDateTime(tbDataDto.getTime()));
        int i = tbSave.saveData(tbDatanew);
//                插入设备数据
        TbOther tbOthernew =new TbOther();
        TbOtherDto tbOtherDto1 = JSON.parseObject(json, TbOtherDto.class);
        BeanUtils.copyProperties(tbOtherDto1, tbOthernew);
        tbOthernew.setTime(dateUtil.getLocalDateTime(tbOtherDto1.getTime()));
        int i2 = tbSave.saveOther(tbOthernew);
    }
    public void savecard(String messageContent){
        String json = null;
        ToJsonUtils toJsonUtils = new ToJsonUtils();
        json = toJsonUtils.tojson(messageContent);
        TbCardDto tbCardDto = JSON.parseObject(json, TbCardDto.class);

        TbCid tbCid = JSON.parseObject(json, TbCid.class);
        Boolean aBoolean1 = tbSave.saveCid(tbCid);

        TbCard tbCardnew = new TbCard();
        BeanUtils.copyProperties(tbCardDto, tbCardnew);
        tbCardnew.setTime(dateUtil.getLocalDateTime(tbCardDto.getTime()));
        int i1 = tbSave.saveCard(tbCardnew);
    }
    public void savewarn(String messageContent){
        String json = null;
        ToJsonUtils toJsonUtils = new ToJsonUtils();
        json = toJsonUtils.tojson(messageContent);
        TbWarningDto tbWarningDto = JSON.parseObject(json, TbWarningDto.class);
        TbWarning tbWarningNew = new TbWarning();
        Long time = tbWarningDto.getTime();

        BeanUtils.copyProperties(tbWarningDto, tbWarningNew);
        tbWarningNew.setTime(dateUtil.getLocalDateTime(tbWarningDto.getTime()));
        int i3 = tbSave.saveWarning(tbWarningNew);
    }
    public Boolean NetConnection(String topic) throws InterruptedException {
        System.out.println("1: now: " + now + "  end: " + end);
        //断网处理,发布
        Duration duration;
        long l = 0;
        if (topic.equals(resume1)) {

            end = LocalDateTime.now();
            end.plusSeconds(1);
            duration = Duration.between(now, end);
            l = duration.toMillis();
            System.out.println("2: now: " + now + "  end: " + end);
            System.out.println(l);
        }
        boolean b = false;
        //发布
        if (l >= 4500) {
            Thread.sleep(300);
            Boolean aBoolean = pubTime(messageContent);
            Thread.sleep(300);
            b = pubilishService.publish(now, end);
        }
        end = LocalDateTime.now();
        now = end;
        System.out.println("3: now: " + now + "  end: " + end);
        return b;
    }

    public Boolean NetConnection2(String topic) throws InterruptedException {
        System.out.println("1: now: " + now2 + "  end: " + end2);
        //断网处理,发布
        Duration duration;
        long l = 0;
        if (topic.equals(resume2)) {
            end2 = LocalDateTime.now();
            end2.plusSeconds(1);
            duration = Duration.between(now2, end2);
            l = duration.toMillis();
            System.out.println("2: now: " + now2 + "  end: " + end2);
            System.out.println(l);
        }
        boolean b = false;
        //发布
        if (l >= 4500) {
            Thread.sleep(300);
            Boolean aBoolean = pubTime(messageContent);
            Thread.sleep(300);
            b = pubilishService.publish(now2, end2);
        }
        end2 = LocalDateTime.now();
        now2 = end2;
        System.out.println("3: now: " + now2 + "  end: " + end2);
        return b;
    }

    public Boolean pubTime(String deviceid) {

        TbControl tbControl = new TbControl();
        tbControl.setDeviceid(Long.valueOf(deviceid));
        tbControl.setStatus("复位");
        pubilishService.pubTime(tbControl);
        return true;
    }


    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("deliveryComplete---------" + token.isComplete());
    }
}
