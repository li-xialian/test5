package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbControl implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户账号
     */
    @TableField("userAccount")
    private String useraccount;

    private Long deviceid;

    /**
     * 开关
     */
    private String swit;

    /**
     * 速度
     */
    private Float speed;

    /**
     * 光照阈值
     */
    private Float light;

    private String status;

    /**
     * 温度阈值
     */
    private Integer th;

    private LocalDateTime time;

    private Integer flag;

    private String fl;

    private String fs;


}
