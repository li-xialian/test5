package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电动自行车id号
     */
    private Long deviceid;
    private Long id;

    /**
     * 电池id
     */
    private Long cardid;

    /**
     * 坐标
     */
    private String x;

    /**
     * 坐标
     */
    private String y;

    /**
     * 位置
     */
    @TableField("location")
    private String  location;

    /**
     * 状态
     */
    private String status;

    /**
     * 信息
     */
    private String massege;


}
