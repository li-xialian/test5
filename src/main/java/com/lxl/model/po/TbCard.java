package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbCard implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long cardid;
    private Long deviceid;
    private Integer id;

    @TableField("V1")
    private Float v1;

    @TableField("V2")
    private Float v2;

    @TableField("T1")
    private Float t1;

    @TableField("T2")
    private Float t2;

    @TableField("I")
    private Float i;

    @TableField("t")
    private Float t;

    private String vstatus;

    private Float cardtotal;
    private Float devicetotal;

    /**
     * 时间
     */
    private LocalDateTime time;


}
