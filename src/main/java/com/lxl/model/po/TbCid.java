package com.lxl.model.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbCid implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long cardid;

    private Long deviceid;

    private String massege;


}
