package com.lxl.model.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbWarning implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long deviceid;
    private Integer id;

    private String status2;
    private String massege;

    private LocalDateTime time;


}
