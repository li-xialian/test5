package com.lxl.model.dto.chart;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
public class SeriesDto<T> {
 private List<T> data;
 private String name;
}
