package com.lxl.model.dto.chart;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Slf4j
public class CharDto {
    private List<SeriesDto> series;
    private List<LocalDateTime> categories;

}
