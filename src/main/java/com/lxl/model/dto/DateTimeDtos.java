package com.lxl.model.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class DateTimeDtos {

    private String start;
    private String end;
    private String id1;
    private String id2;
}
