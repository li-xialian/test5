package com.lxl.model.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Option {
    private String label;
    private Integer Value;
}
