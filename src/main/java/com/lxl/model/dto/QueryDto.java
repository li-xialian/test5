package com.lxl.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QueryDto implements Serializable {


    private Long cardid;
    private Long deviceid;
    private Integer id;
    private Float v1;

    private Float v2;

    private Float t1;

    private Float t2;

    private Float i;

    private Float t;

    private Integer vstatus;

    private Float cardtotal;
    private Float devicetotal;

    /**
     * 时间
     */
    private LocalDateTime time;


}
