package com.lxl.model.dto.mqttMas;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 */
@Data
public class TbOtherDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Long cardid;

    private Long deviceid;

    private String x;

    private String y;

    private String a;

    private String b;

    private Long time;


}
