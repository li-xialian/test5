package com.lxl.model.dto.mqttMas;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 */
@Data
public class TbControlDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户账号
     */
    private String useraccount;

    private Long deviceid;

    /**
     * 开关
     */
    private String swit;

    /**
     * 速度
     */
    private Float speed;

    /**
     * 光照阈值
     */
    private Float light;
    private Integer th;
    private String status;

    private Long time;

    private Integer flag;

    private String fl;

    private String fs;


}
