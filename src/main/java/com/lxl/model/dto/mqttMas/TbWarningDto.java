package com.lxl.model.dto.mqttMas;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 */
@Data
public class TbWarningDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Long deviceid;

    private String status2;
    private String massege;

    private Long time;


}
