package com.lxl.model.dto.mqttMas;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 */
@Data

public class TbCardDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Long cardid;
    private Long deviceid;

    private Float v1;

    private Float v2;

    private Float t1;

    private Float t2;

    private Float i;

    private Float t;

    private String vstatus;

    private Float cardtotal;

    /**
     * 时间
     */
    private Long time;


}
