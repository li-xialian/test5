package com.lxl.model.dto.mqttMas;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author lxl
 * @since 2023-08-23
 */
@Data

public class TbDataDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Long deviceid;

    private Float devicetotal;

    /**
     * 温度
     */
    private Float t;

    /**
     * 时间
     */
    private Long time;

    /**
     * 实时速度
     */
    private Float speed;

    /**
     * 光照
     */
    private Float light;
    private String x;
    private String y;


}
