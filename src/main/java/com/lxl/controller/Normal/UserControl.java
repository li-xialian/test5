package com.lxl.controller.Normal;

import com.lxl.model.po.TbUser;
import com.lxl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class UserControl {

   @Autowired
    UserService userService;

    /**
     *
     * @return user.id
     */
    @PostMapping("/selectUser")
    public String selectUser(@RequestBody TbUser tbUser) {
        String s = userService.selectUser(tbUser);

        return s;
    }

    @PostMapping("/addUesr")
    public String addUesr(@RequestBody TbUser tbUser) {
        String s = userService.addUesr(tbUser);
        return s;

    }
}
