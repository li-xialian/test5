package com.lxl.controller.Normal;

import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.po.TbControl;
import com.lxl.mqtt.MqttMessageService;
import com.lxl.service.TbSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class PubulishControl {

    @Autowired
    MqttMessageService mqttMessageService;

    @Autowired
    TbSaveService tbSaveService;
    @Value("${topic.control}")
    private  String topic;

    @PostMapping("/send")
    public String sendcommand(@RequestBody TbControl tb){
        if(!"无用户".equals(tb.getUseraccount())&&null!=tb.getUseraccount()&&""!=tb.getUseraccount()) {
            if (null != tb.getDeviceid()) {
                mqttMessageService.publish(topic, tb);
                LocalDateTime localTime = LocalDateTime.now();
                tb.setTime(localTime);
                tbSaveService.saveControl(tb);
            }
            return "success";
        }
        else{
            return "false";
        }
    }

    @PostMapping("/sendTime")
    public boolean sendTime(@RequestBody DateTimeDtos dateTimeDtos){
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        //转换格式
        String s="";
        String[] split = start.split(":");
        s="h1:"+split[0]+",m1:"+split[1]+",";
        String[] split1 = end.split(":");
        s=s+"h2:"+split1[0]+",m2:"+split1[1];
        System.out.println(s);
        mqttMessageService.publish(topic, s);
        return true;
    }
}
