package com.lxl.controller.Normal;

import com.lxl.model.dto.chart.CharDto;
import com.lxl.service.ChartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class ChartControl {

    @Autowired
    ChartService chartService;

    @GetMapping("/chat1")
    public CharDto getchat1(Long id) {
        CharDto chat = chartService.chat1(6,id);
        return chat;
    }

    @GetMapping("/chat2")
    public CharDto getchat2( Long id) {
        CharDto chat = chartService.chat2(6,id);
       return chat;
    }


}
