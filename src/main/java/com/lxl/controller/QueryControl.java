package com.lxl.controller;

import com.lxl.model.dto.Option;
import com.lxl.model.dto.QueryDto;
import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.po.TbCard;
import com.lxl.model.po.TbData;
import com.lxl.model.po.TbDevice;
import com.lxl.model.po.TbOther;
import com.lxl.service.QueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class QueryControl {
    @Autowired
    QueryService queryService;

    //历史查询
    @PostMapping("/getTb1")
    public PageResult<TbCard> getBikeSpeed(PageParams pageParams, @RequestBody DateTimeDtos dateTimeDtos) {
        PageResult<TbCard> b = queryService.getData(pageParams,dateTimeDtos);
        return b;
    }
    @PostMapping("/getTb2")
    public PageResult<TbCard> getBattery(PageParams pageParams, @RequestBody DateTimeDtos dateTimeDtos) {
        PageResult<TbCard> b = queryService.getCard(pageParams,dateTimeDtos);
        return b;
    }
    @PostMapping("/getTb3")
    public PageResult<TbOther> gettb3(PageParams pageParams, @RequestBody DateTimeDtos dateTimeDtos) {
        PageResult<TbOther> b = queryService.getOther(pageParams,dateTimeDtos);
        return b;
    }
    @GetMapping("/getOption1")
    public List<Option> getOption1() {
        List<Option> p = queryService.getOption1();
        return p;
    }
    @GetMapping("/getOption2")
    public List<Option> getOption2() {
        List<Option> p = queryService.getOption2();
        return p;
    }
//    查询对应设备与卡号的饮水总量
    @PostMapping ("/geta")
    public TbCard geta(String id1,String id2,@RequestBody DateTimeDtos dateTimeDtos) {
        TbCard p = queryService.geta(id1,id2,dateTimeDtos);
        return p;
    }


    @GetMapping("/getOption11")
    public List<Option> getOption11() {
        List<Option> p = queryService.getOption11();
        return p;
    }
    @GetMapping("/getOption22")
    public List<Option> getOption22() {
        List<Option> p = queryService.getOption22();
        return p;
    }

////这个得优化掉
//    @GetMapping("/getOptionAll")
//    public List<Option> getOptionAll() {
//        List<Option> p = queryService.getOptionAll();
//        return p;
//    }
}
