package com.lxl.controller;

import com.lxl.model.dto.pages.PageParams;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.po.TbDevice;
import com.lxl.model.po.TbWarning;
import com.lxl.service.ContentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lxl
 */
@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class ContentController {
    @Autowired
    ContentService contentService;

    //index
    @PostMapping("/searchIndex")
    public PageResult<TbDevice> selectSearch(PageParams pageParams,@RequestBody(required = false) String keyword) {
        PageResult<TbDevice> p= contentService.selectSearch(pageParams, keyword);
        return  p;
    }

    @PostMapping("/addIndex")
    public Boolean addIndex(@RequestBody TbDevice tb) {
        Boolean b = contentService.addIndex(tb);
        return b;
    }

    @DeleteMapping ("/delIndex")
    public Boolean delIndex(String deviceid) {
        Boolean b = contentService.delIndex(deviceid);
        return b;
    }

    @PutMapping("/updateIndex")
    public Boolean updataIndex(@RequestBody TbDevice tb){
        Boolean b = contentService.updataIndex(tb);
        return b;
    }

    //运行状况
    @PostMapping("/getWarning")
    public PageResult<TbWarning> getWarning(PageParams pageParams,@RequestBody DateTimeDtos dateTimeDtos) {
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        PageResult<TbWarning> s = contentService.getVStatus(pageParams, start, end);
        return s;
    }


}
