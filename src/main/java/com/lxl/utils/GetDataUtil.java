package com.lxl.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class GetDataUtil {

    public String getStringData(){
        //创建Calendar对象
        Calendar cal=Calendar.getInstance();

        //用Calendar类提供的方法获取年、月、日、时、分、秒
        int year  =cal.get(Calendar.YEAR);   //年
        int month =cal.get(Calendar.MONTH)+1;  //月  默认是从0开始  即1月获取到的是0
        int day   =cal.get(Calendar.DAY_OF_MONTH);  //日，即一个月中的第几天
        int hour  =cal.get(Calendar.HOUR_OF_DAY);  //小时
        int minute=cal.get(Calendar.MINUTE);   //分
        int second=cal.get(Calendar.SECOND);  //秒

        //拼接成字符串输出
        String date="y:"+year+","+"m:"+month+","+"d:"+day+","+"h:"+hour+","+"m:"+minute+","+"s:"+second;
        System.out.println("当前时间是---->"+date);
        return date;
    }

    public int getMillisData(){
        //创建Calendar对象
        Calendar cal=Calendar.getInstance();

        //用Calendar类提供的方法获取年、月、日、时、分、秒
        int hour  =cal.get(Calendar.HOUR_OF_DAY);  //小时
        int minute=cal.get(Calendar.MINUTE);   //分
        int second=cal.get(Calendar.SECOND);  //秒

        int millis=hour*3600+minute*60+second;
        return millis;
    }
    public LocalDateTime getLocalDateTime(Long time) {
        int h, m, s;
        Date data = new Date();
        LocalDateTime localDateTime = LocalDateTime.now();
        if (null != time) {
            h = (int) (time / 3600);
            m = (int) (time / 60 % 60);
            s = (int) (time % 60);
            data.setHours(h);
            data.setMinutes(m);
            data.setSeconds(s);
            Instant instant = data.toInstant();
            localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            System.out.println(localDateTime);
        }
        return localDateTime;
    }
}
