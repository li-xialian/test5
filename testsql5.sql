/*
 Navicat Premium Data Transfer

 Source Server         : data1
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : localhost:3306
 Source Schema         : testsql5

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 25/08/2023 12:33:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_card
-- ----------------------------
DROP TABLE IF EXISTS `tb_card`;
CREATE TABLE `tb_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardid` bigint(20) NULL DEFAULT NULL,
  `V1` float NULL DEFAULT NULL,
  `V2` float NULL DEFAULT NULL,
  `T1` float NULL DEFAULT NULL,
  `T2` float NULL DEFAULT NULL,
  `I` float NULL DEFAULT NULL,
  `T` float NULL DEFAULT NULL,
  `vstatus` int(11) NULL DEFAULT NULL,
  `cardtotal` float NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_cid
-- ----------------------------
DROP TABLE IF EXISTS `tb_cid`;
CREATE TABLE `tb_cid`  (
  `cardid` bigint(11) NOT NULL,
  `deviceid` bigint(11) NULL DEFAULT NULL,
  `massege` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cardid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_control
-- ----------------------------
DROP TABLE IF EXISTS `tb_control`;
CREATE TABLE `tb_control`  (
  `userAccount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `deviceid` bigint(20) NULL DEFAULT NULL,
  `swit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开关',
  `speed` float NULL DEFAULT NULL COMMENT '速度',
  `light` float NULL DEFAULT NULL COMMENT '光照阈值',
  `status` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `th` int(11) NULL DEFAULT NULL COMMENT '温度阈值',
  `time` datetime NULL DEFAULT NULL,
  `flag` int(11) NULL DEFAULT NULL,
  `fl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_data`;
CREATE TABLE `tb_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceid` bigint(20) NOT NULL,
  `devicetotal` float NULL DEFAULT NULL,
  `t` float NULL DEFAULT NULL COMMENT '温度',
  `time` datetime NULL DEFAULT NULL COMMENT '时间',
  `speed` float NULL DEFAULT NULL COMMENT '实时速度',
  `light` float NULL DEFAULT NULL COMMENT '光照',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_device
-- ----------------------------
DROP TABLE IF EXISTS `tb_device`;
CREATE TABLE `tb_device`  (
  `deviceid` bigint(20) NOT NULL COMMENT '电动自行车id号',
  `cardid` bigint(20) NULL DEFAULT NULL COMMENT '电池id',
  `x` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标',
  `y` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标',
  `location` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '位置',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `massege` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '信息'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份',
  PRIMARY KEY (`account`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_warning
-- ----------------------------
DROP TABLE IF EXISTS `tb_warning`;
CREATE TABLE `tb_warning`  (
  `deviceid` bigint(20) NOT NULL,
  `status` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
