package com.lxl.service.impl;

import com.lxl.model.po.TbOther;
import com.lxl.mapper.TbOtherMapper;
import com.lxl.service.TbOtherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxl
 */
@Slf4j
@Service
public class TbOtherServiceImpl extends ServiceImpl<TbOtherMapper, TbOther> implements TbOtherService {

}
