package com.lxl.service;

import com.lxl.model.po.TbOther;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxl
 * @since 2023-09-03
 */
public interface TbOtherService extends IService<TbOther> {

}
