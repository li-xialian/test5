package com.lxl.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lxl.service.TbOtherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lxl
 */
@Slf4j
@RestController
@RequestMapping("tbOther")
public class TbOtherController {

    @Autowired
    private TbOtherService  tbOtherService;
}
