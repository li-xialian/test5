package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 */
@Data
@TableName("tb_other")
public class TbOther implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Long cardid;

    private Long deviceid;

    private String x;

    private String y;

    private String a;

    private String b;

    private LocalDateTime time;


}
